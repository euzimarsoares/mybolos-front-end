import {http} from './config'

export default{
    listar:() =>{
        return http.get('produto/listar')
    },
    listarComplemento:(id)=>{
        return http.get('produto/listarComplementos/'+id)
    }
}