import {http} from './config'

export default{
    listar:() =>{
        return http.get('pedido/listar')
    },
    add:(dados) =>{
        return http.post('pedido/add', dados)
    }
}