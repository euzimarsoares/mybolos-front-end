import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home/home'
import principal from '@/components/principal/principal'
import meusPedidos from '@/components/meusPedidos/meusPedidos'
import meuPerfil from '@/components/perfil/meuPerfil'
import cadProduto from '@/components/cadastros/produto/cadProduto'
import cadComplemento from '@/components/cadastros/complemento/cadComplemento'
import login from '@/components/sessao/login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'principal',
      component: principal
    },
	  {
      path: '/pedidos',
      component: home,
      children: [
        {
          path: '/',
          name: 'meusPedidos',
          component: meusPedidos
        },
        {
          path: '/perfil',
          name: 'meuPerfil',
          component: meuPerfil
        },
        {
          path: '/produtos',
          name: 'cadProduto',
          component: cadProduto
        },
        {
          path: '/complementos',
          name: 'cadComplemento',
          component: cadComplemento
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: login
    }
  ]
})
